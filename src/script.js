class MessageStore 
{
    constructor() 
    {
        this.messagesBuffer = "";
    }  
    addMessage(message) 
    {
        this.messagesBuffer += " " + message
    }  
    getMessages() 
    {
        return this.messagesBuffer
    } 
}

class MessageParser 
{
    constructor() 
    {
        this.currentSentiment = {};
        this.emotionLevel = 0;
    }
    parseResponseBody(responseBodyRaw) 
    {
        let responseBody = responseBodyRaw

        this.currentSnetiment = responseBody
    }
    getCurrentSentiment() 
    {
        return this.currentSnetiment
    }
}

let messagesBuffer = new MessageStore();
let parser = new MessageParser();

function showContent() {
    document.getElementById("bankTitleContainer").classList.add("d-none");
    document.getElementById("bankTitleContainer").classList.remove("d-flex");

    document.getElementById("messagePillClient").classList.remove("d-none");
    document.getElementById("messagePillBank").classList.remove("d-none");

    document.getElementById("bankTitle").style.filter = 'blur(0px)';
    document.getElementById("bankScreen").style.filter = 'blur(0px)';
    document.getElementById("bankScreen").style.opacity = 1;
    document.getElementById("analisysScreen").style.filter = 'blur(0px)';
    document.getElementById("analisysScreen").style.opacity = 1;
}

function scrollClient() {
    const block1 = document.getElementById('clientScreen');
    block1.scrollTo(0, block1.scrollHeight);
}
    
function scrollBank() {
    const block2 = document.getElementById('bankScreen');
    block2.scrollTo(0, block2.scrollHeight);
}

// function scrollDiv() {
//     console.log("scrollu");
    
//     var block = document.getElementById("messagePillClient");
//     block.scrollTo(0, block.scrollHeight);

//     console.log(block.scrollTo);
//     console.log(block.scrollHeight);
// }

function addMessagesClient() {
    if (document.getElementById("textAreaMessageClient").value != '' || 
        document.getElementById("textAreaMessageBank").value != '')
    {
        var text = document.getElementById("textAreaMessageClient").value;
        messagesBuffer.addMessage(text);

        if (document.getElementById("bankTitleContainer").classList.contains("d-none") == 0)
        {
            showContent();            
            $('#firstMessageParClient').html(text);
            $('#firstMessageParBank').html(text);
        }
        var newElemClient = document.getElementById("messagePillClient");
        newElemClient.innerHTML +=  '<div class=" row justify-content-end mr-0" >'
                            + '<ul class="list-unstyled" id="messageList">'
                            + '<li class="d-flex justify-content-between mb-0">' 
                            + '<div class="card border-light" style="max-width: 14rem; border-radius: 15px;">'
                            + '<div class="mt-3 ml-3 mr-3">'
                            + '<p>'
                            + text
                            + '</p> </div> </div> </li> </ul> </div>';

        var newElemBank = document.getElementById("messagePillBank");
        newElemBank.innerHTML +=  '<div class=" row justify-content-start ml-3">'
                            + '<ul class="list-unstyled" id="messageList">'
                            + '<li class="d-flex justify-content-between mb-0">' 
                            + '<div class="card border-light" style="max-width: 14rem; border-radius: 15px;">'
                            + '<div class="mt-3 ml-3 mr-3">'
                            + '<p>'
                            + text
                            + '</p> </div> </div> </li> </ul> </div>';

        document.getElementById("textAreaMessageClient").value = '';
        scrollClient();
        scrollBank();
        sendToServer();
        console.log(text);
    }
}

function addMessagesClientOnButton(event) {
    event.preventDefault();
    if (event.keyCode === 13) { 
        addMessagesClient() 
    };
}

function addMessagesBank() {
    if ((document.getElementById("textAreaMessageClient").value != '' || 
        document.getElementById("textAreaMessageBank").value != '') && 
        document.getElementById("bankTitleContainer").classList.contains("d-none"))
    {
        var text = document.getElementById("textAreaMessageBank").value;

        document.getElementById("messagePillClient").classList.remove("d-none");
        document.getElementById("messagePillBank").classList.remove("d-none");

        var newElemBank = document.getElementById("messagePillClient");
            newElemBank.innerHTML +=  '<div class=" row justify-content-start ml-3">'
                                + '<ul class="list-unstyled" id="messageList">'
                                + '<li class="d-flex justify-content-between mb-0">' 
                                + '<div class="card border-light" style="max-width: 14rem; border-radius: 15px;">'
                                + '<div class="mt-3 ml-3 mr-3">'
                                + '<p>'
                                + text
                                + '</p> </div> </div> </li> </ul> </div>';

        var newElemClient = document.getElementById("messagePillBank");
        newElemClient.innerHTML +=  '<div class=" row justify-content-end mr-0">'
                            + '<ul class="list-unstyled" id="messageList">'
                            + '<li class="d-flex justify-content-between mb-0">' 
                            + '<div class="card border-light" style="max-width: 14rem; border-radius: 15px;">'
                            + '<div class="mt-3 ml-3 mr-3">'
                            + '<p>'
                            + text
                            + '</p> </div> </div> </li> </ul> </div>';

        document.getElementById("textAreaMessageBank").value = '';
        scrollClient();
        scrollBank();
    }
}

function  addMessagesBankOnButton(event) {
    event.preventDefault();
    if (event.keyCode === 13) { 
        addMessagesBank() 
    };
}

function sendToServer() {
    var url = "https://ivoya.pythonanywhere.com/predict";

    var xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
        console.log(xhr.statusText);
        var jsonResponse = xhr.response;
        console.log(jsonResponse);
        parser.parseResponseBody(jsonResponse);
        putResults();
    }};

    var data = `{
    "message": "${messagesBuffer.getMessages()}"
    }`;

    xhr.send(data);
}

function putResults() {
    var sentiment = parser.getCurrentSentiment();
    console.log(sentiment['sentiment'].positive);
    var pos = sentiment['sentiment'].positive;
    var neg = sentiment['sentiment'].negative;
    var neu = 1 - pos - neg;

    var state = 0;

    if (neg >= 0.7)
        state = 1;
    else if (neg >= 0.5 && neg < 0.7)
        state = 2;
    else if (pos >= 0.5 && pos < 0.7)
        state = 4;
    else if (pos >= 0.7)
        state = 5;
    else
        state = 3;

    document.getElementById("emoji").innerHTML  = '<img src="images/'
                                                + state
                                                + '.png" width=40px height="40px" class="mt-3 mr-2"/>';
    document.getElementById("recomendations").innerHTML = '<div class="card mt-3" style="border-radius: 8px;">'
                                                        + '<div class="card-body p-3">'
                                                        + sentiment['recommendation'][0]
                                                        + '</div> </div> <div class="card mt-3" style="border-radius: 8px;">'
                                                        + '<div class="card-body p-3">'
                                                        + sentiment['recommendation'][1]
                                                        + '</div> </div> <div class="card mt-3" style="border-radius: 8px;">'
                                                        + '<div class="card-body p-3">'
                                                        + sentiment['recommendation'][2]
                                                        + '</div> </div>';

    document.getElementById("rectPos").style.width = Math.floor(pos * 80) + 'px';
    document.getElementById("rectNeg").style.width = Math.floor(neg * 80) + 'px';
    document.getElementById("rectNeu").style.width = 80 - Math.floor(pos * 80) - Math.floor(neg * 80) + 'px';
    
    document.getElementById("procPos").innerHTML = Math.floor(pos * 100) + '%';
    document.getElementById("procNeg").innerHTML = Math.floor(neg * 100) + '%';
    document.getElementById("procNeu").innerHTML = 100 - Math.floor(pos * 100) - Math.floor(neg * 100) + '%';
}